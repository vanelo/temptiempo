/*
 *  TempTiempo.ino
 *
 *  A traves del Monitor Serial el usuario puede solicitar la lectura de Temperatura y
 *  Humedad. La solicitud se hace ingresando la letra T. La salida se muestra en el Monitor
 *  Serial incluyendo ademas los datos de tiempo correspondiente
 */

#include <TimeLib.h>
#include <DHT11.h>

bool k = false;					//bandera para mostrar el mensaje de usuario una vez
int tempSensor = 5;				//pin conectado al sensor de temperatura
DHT11 dht11(tempSensor);		//inicializar el sensor

void setup()  {
	Serial.begin(115200);			//inicializar el Serial
	setTime(1536170400);		//Hora de inicio configurada 05/09/2018 14:00:00
}

void loop(){ 
  if(k){
    Serial.println("Ingrese T para ver datos de temperatura y humedad");
    k = false;  
  }
	if(Serial.available()){
		if(Serial.find("T")){

			float temp, hum;						//variables de humedad y temperatura

			if((dht11.read(hum, temp)) == 0){		// Si devuelve 0 es que ha leido bien
				dht11DataDisplay(hum);					//funcion que imprime los datos del sensor dht11
				digitalClockDisplay();				// funcion que imprime hora, dia, mes y año
			}

			k = false;	 
			delay(DHT11_RETRY_DELAY);				//delay minimo necesario para la lectura sensor dht11

		}
	}
}

void dht11DataDisplay(float hum, float temp){							//funcion que imprime los datos del sensor dht11
	Serial.println("");
	Serial.print("Temperatura: ");
	Serial.print(temp);
	Serial.print(" C")
	Serial.print(" | Humedad: ");
	Serial.print(hum);
	Serial.println(" %");
  Serial.println("");
}

void digitalClockDisplay(){							// funcion que imprime hora, dia, mes y año
	Serial.print("Leido el => ");

	Serial.print("Dia : ");
	Serial.print(day());
	Serial.print(" | Mes : ");
	Serial.print(month());
	Serial.print(" | Anho : "); 
	Serial.print(year());

	Serial.print(" | Hora : ");
	Serial.print(hour());
	printDigits(minute());
	printDigits(second());
	Serial.println("");
  	Serial.println("");
}

void printDigits(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	Serial.print(":");
	if(digits < 10)
		Serial.print('0');
	Serial.print(digits);
}